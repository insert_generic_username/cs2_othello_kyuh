1. (Solo work, question does not apply)

2. 

To make this tournament worthy: In contrast to the heuristic implemented in the last assignment, a basic version of minimax was used (1 level of recursion), and the same heuristic was used to evaluate the opponent move. In theory, this should work better than the previous heuristic-only strategy.

Long answer: I'm reaching the end of a very long all-nighter, so I didn't have the chance to do things that I would otherwise have liked to do. I suppose I could classify these as "ideas 'tried' but didn't work out due to external factors".

The best way to play this game is likely to use a high level of recursion with minimax, or more preferably, alpha-beta pruning. A good way to set these apart: Use some threading library (e.g. pthreads) to allow 8 concurrent explorations of the tree (because, assuming you're running on the more powerful Annenberg machines, they will have 8 cores). This should result in a nice speedup. (Interestingly, the combined alpha-beta pruning with threading was used in a similarly structured problem in CS 38 last year.) 

In addition, one could in theory use vector operations (again, assuming Annenberg computers) to gain a nice ~3-4x speedup when calculating scores (this wasn't exactly taught in class, but it is something you can do; see CS 101c HPC for more details).

Finally, one could use reinforcement learning and "train" othello players by engaging two computers in random moves against each other. Literature varies as to the success of this method.