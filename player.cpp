#include "player.h"
#include <climits>

// This is a small change to the player.cpp file

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     board = new Board();
     this->side = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

int getScore(Board *board, Side side){

    int score = board->count(side);


    for (int i = 0; i < BOARD_LENGTH; i++){
        for (int j = 0; j < BOARD_LENGTH; j++){

                //Check if move takes adjacent to corner
                if ((i == 0 && j == 1) || (i == 1 && j == 0) || (i == 1 && j == 1)
                    || (i == 0 && j == BOARD_LENGTH - 2)
                    || (i == 1 && j == BOARD_LENGTH - 1)
                    || (i == 1 && j == BOARD_LENGTH - 2)
                    || (i == BOARD_LENGTH - 2 && j == 0)
                    || (i == BOARD_LENGTH - 2 && j == 1)
                    || (i == BOARD_LENGTH - 1 && j == 1)
                    || (i == BOARD_LENGTH - 2 && j == BOARD_LENGTH - 1)
                    || (i == BOARD_LENGTH - 2 && j == BOARD_LENGTH - 2)
                    || (i == BOARD_LENGTH - 1 && j == BOARD_LENGTH - 2))
                    score /= 3;


                //Check if move takes a corner
                if (i == j && (i == 0 || i == BOARD_LENGTH - 1))
                    score *= 20;




        }
    }

    return score;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Side other = (side == BLACK) ? WHITE : BLACK;

    board->doMove(opponentsMove, other);


    Move *current_best_move = NULL;
    int current_best_score = INT_MIN;

    //Select the first space possible
    for (int i = 0; i < BOARD_LENGTH; i++){
        for (int j = 0; j < BOARD_LENGTH; j++){

            Board *copy = board->copy();
            Move *m = new Move(i, j);

            if (copy->checkMove(m, side)){
                copy->doMove(m, side);

                Move *worst_opponent_move_possible = NULL;
                int worst_opcause_score = INT_MAX;
                
                for (int k = 0; k < BOARD_LENGTH; k++){
                    for (int l = 0; l < BOARD_LENGTH; l++){
                        Board *copy2 = copy->copy();
                        Move *m2 = new Move(k, l);

                        if (copy2->checkMove(m2, other)){
                            copy2->doMove(m2, other);

                            int s = getScore(copy2, side);

                            if (s < worst_opcause_score){
                                worst_opcause_score = s;
                                worst_opponent_move_possible = m2;
                            }
                        }



                    }
                }

                if (worst_opcause_score > current_best_score){
                    current_best_score = worst_opcause_score;
                    current_best_move = m;
                }
            }

        }
    }

    board->doMove(current_best_move, side);

    return current_best_move;
}
